# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151010141233) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "application_forms", force: :cascade do |t|
    t.string   "user_id"
    t.integer  "status",                default: 0
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.text     "notes"
    t.text     "inauguration_feedback"
  end

  create_table "enrollments", force: :cascade do |t|
    t.integer  "application_form_id"
    t.integer  "project_id"
    t.boolean  "team_leader",         default: false
    t.boolean  "spin_off",            default: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.boolean  "accepted",            default: false
  end

  add_index "enrollments", ["application_form_id"], name: "index_enrollments_on_application_form_id", using: :btree
  add_index "enrollments", ["project_id"], name: "index_enrollments_on_project_id", using: :btree

  create_table "meeting_questions", force: :cascade do |t|
    t.text     "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "memberships", force: :cascade do |t|
    t.integer "user_id",                null: false
    t.integer "project_id",             null: false
    t.integer "role",       default: 0, null: false
  end

  add_index "memberships", ["project_id"], name: "index_memberships_on_project_id", using: :btree
  add_index "memberships", ["user_id"], name: "index_memberships_on_user_id", using: :btree

  create_table "projects", force: :cascade do |t|
    t.string  "name"
    t.text    "description"
    t.integer "status",      default: 0, null: false
  end

  create_table "time_slots", force: :cascade do |t|
    t.datetime "start_time"
    t.datetime "end_time"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "application_form_id"
  end

  add_index "time_slots", ["application_form_id"], name: "index_time_slots_on_application_form_id", using: :btree

  add_foreign_key "enrollments", "application_forms"
  add_foreign_key "enrollments", "projects"
  add_foreign_key "time_slots", "application_forms"
end
