class AddDeafultStatusForProjects < ActiveRecord::Migration
  def up
    ActiveRecord::Base.connection.execute('UPDATE projects SET status = 0 WHERE status IS NULL')

    change_column :projects, :status, :integer, default: 0, null: false
  end

  def down
    change_column :projects, :status, :integer
  end
end
