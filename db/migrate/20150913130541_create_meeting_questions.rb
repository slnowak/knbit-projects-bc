class CreateMeetingQuestions < ActiveRecord::Migration
  def change
    create_table :meeting_questions do |t|
      t.text :content

      t.timestamps null: false
    end
  end
end
