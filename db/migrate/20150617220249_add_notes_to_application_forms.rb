class AddNotesToApplicationForms < ActiveRecord::Migration
  def change
    add_column :application_forms, :notes, :text
  end
end
