class AddAcceptedToEnrollments < ActiveRecord::Migration
  def change
    add_column :enrollments, :accepted, :boolean, default: false
    remove_column :enrollments, :status
  end
end
