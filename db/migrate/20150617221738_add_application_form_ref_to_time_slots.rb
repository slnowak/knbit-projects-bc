class AddApplicationFormRefToTimeSlots < ActiveRecord::Migration
  def change
    add_reference :time_slots, :application_form, index: true, foreign_key: true
    remove_reference :time_slots, :meeting
  end
end
