class ChangeUserIdInApplicationFormsFromIntToString < ActiveRecord::Migration
  def change
    change_column :application_forms, :user_id, :string
  end
end
