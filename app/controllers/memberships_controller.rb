class MembershipsController < ApplicationController
  before_action :set_project, only: [:create, :update, :destroy]
  before_action :set_membership, only: [:update, :destroy]
  before_action :authorize_admin

  def show
  end

  def create
    set_project

    begin
      @membership = @project.memberships.create(membership_create_params)
      status = :ok
    rescue ActiveRecord::RecordInvalid
      status = :unprocessable_entity
    end

    render :show, status: status
  end

  def destroy
    @membership.destroy
    head :ok
  end

  def update
    if @membership.update(membership_update_params)
      render :show, status: :ok
    else
      render json: { errors: @membership.errors.messages }, status: :unprocessable_entity
    end
  end

  private

  def set_project
    @project = Project.includes(:memberships).find(params[:project_id])
  rescue ActiveRecord::RecordNotFound
    head :not_found
  end

  def set_membership
    @membership = @project.memberships.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    head :not_found
  end

  def membership_create_params
    params.require(:membership).permit(:user_id, :role, :status)
  end

  def membership_update_params
    params.require(:membership).permit(:role, :status)
  end
end
