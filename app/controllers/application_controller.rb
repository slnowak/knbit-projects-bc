class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session

  before_action :authenticate_user

  private

  def authenticate_user
    token = get_token

    @user_id = AuthService.authenticate(token)
    UserService.prepare(@user_id, token)
    head :unauthorized unless @user_id
  end

  def authorize_admin
    token = get_token

    head :forbidden unless AuthService.authorize(token, 'ADMIN')
  end

  def get_token
    request.headers['X-Authentication']
  end

  def validate_user(user_id)
    head :forbidden unless (@user_id == user_id || AuthService.authorize(get_token, 'ADMIN'))
  end
end
