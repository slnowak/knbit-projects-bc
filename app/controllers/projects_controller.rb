class ProjectsController < ApplicationController
  before_action :set_project, only: [:show, :update, :destroy, :change_status]
  before_action :authorize_admin, only: [:create, :update, :destroy]

  def index
    @projects = Project.all
  end

  def show
    @user_store = UserStore.new
  end

  def create
    @project = Project.new(project_params)

    if @project.save
      render :show, status: :ok
    else
      render json: { errors: @project.errors.messages }, status: :unprocessable_entity
    end
  end

  def update
    if @project.update(project_params)
      render :show, status: :ok
    else
      render json: { errors: @project.errors.messages }, status: :unprocessable_entity
    end
  end

  def destroy
    @project.destroy

    head :ok
  end

  def change_status
    @project.update(status: params[:status])
    render :show, status: :ok
  rescue ArgumentError
    head :unprocessable_entity
  end

  private

  def set_project
    @project = Project.includes(:memberships, enrollments: :application_form).find(params[:id])
  rescue ActiveRecord::RecordNotFound
    head :not_found
  end

  def project_params
    params.require(:project).permit(:name, :description)
  end
end