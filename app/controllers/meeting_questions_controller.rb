class MeetingQuestionsController < ApplicationController
  before_action :set_meeting_question, only: [:show, :update, :destroy]

  def index
    @meeting_questions = MeetingQuestion.all
  end

  def show
  end

  def create
    @meeting_question = MeetingQuestion.new(meeting_question_params)

    if @meeting_question.save
      render :show, status: :ok
    else
      render json: { errors: @meeting_question.errors.messages }, status: :unprocessable_entity
    end
  end

  def update
    if @meeting_question.update(meeting_question_params)
      render :show, status: :ok
    else
      render json: { errors: @meeting_question.errors.messages }, status: :unprocessable_entity
    end
  end

  def destroy
    @meeting_question.destroy

    head :ok
  end

  def change_status
    @meeting_question.update(status: params[:status])
    render :show, status: :ok
  rescue ArgumentError
    head :unprocessable_entity
  end

  private

  def set_meeting_question
    @meeting_question = MeetingQuestion.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    head :not_found
  end

  def meeting_question_params
    params.require(:meeting_question).permit(:content)
  end
end