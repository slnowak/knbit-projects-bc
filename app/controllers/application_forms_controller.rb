class ApplicationFormsController < ApplicationController

  before_action :set_application_form, only: [:show, :update, :destroy, :set_meeting, :change_status]
  before_action :set_time_slot, only: [:set_meeting]
  before_action :authorize_admin, only: [:index, :show, :change_status]

  def index
    @user_store = UserStore.new
    @application_forms = ApplicationForm.all.includes(:meeting_time, enrollments: :project)
  end

  def show
  end

  def new
    @application_form = ApplicationForm.new
    render :show
  end

  def current
    @application_form = ApplicationForm.where(user_id: @user_id).first!
    render :show
  rescue ActiveRecord::RecordNotFound
    head :not_found
  end

  def create
    @application_form = ApplicationForm.new(application_form_create_params)

    if @application_form.save
      render :show, status: :ok
    else
      render json: {errors: @application_form.errors.messages}, status: :unprocessable_entity
    end
  end

  # TODO move to EnrollmentsController?
  def update
    validate_user(@application_form.user_id)

    if params[:application_form][:enrollments_attributes].present?
      @application_form.enrollments.destroy_all
    end

    if @application_form.update(application_form_update_params)
      @application_form.assign_meeting
      render :show, status: :ok
    else
      render json: {errors: @application_form.errors.messages}, status: :unprocessable_entity
    end
  end

  def destroy
    validate_user(@application_form.user_id)

    @application_form.destroy

    head :ok
  end

  def set_meeting
    validate_user(@application_form.user_id)
    if @application_form.set_meeting(@time_slot)
      render :show, status: :ok
    else
      render json: {errors: 'Time Slot already taken.'}, status: :unprocessable_entity
    end
  end

  def change_status
    @application_form.update(status: params[:status])
    render :show, status: :ok
  rescue ArgumentError
    head :unprocessable_entity
  end

  private

  def set_application_form
    @application_form = ApplicationForm.includes(:meeting_time, enrollments: :project).find(params[:id])
  rescue ActiveRecord::RecordNotFound
    head :not_found
  end

  def set_time_slot
    @time_slot = TimeSlot.find_by!(id: params[:time_slot_id])
  rescue ActiveRecord::RecordNotFound
    head :not_found
  end

  # TODO - make it more DRY
  def application_form_create_params
    enrollments_attributes = set_enrollments_attributes
    params.require(:application_form).permit(:user_id, :inauguration_feedback, :time_slot_id,
                                             enrollments_attributes: enrollments_attributes)
  end

  # TODO - add authorization on update params
  def application_form_update_params
    enrollments_attributes = set_enrollments_attributes
    params.require(:application_form).permit(:user_id, :notes, :time_slot_id,
                                             enrollments_attributes: enrollments_attributes)
  end

  def set_enrollments_attributes
    attributes = [:project_id, :team_leader, :spin_off]
    attributes << :accepted if AuthService.authorize(get_token, 'ADMIN')
    attributes
  end
end