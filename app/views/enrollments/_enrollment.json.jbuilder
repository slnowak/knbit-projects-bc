json.id enrollment.id
json.project do json.partial! enrollment.project end
json.team_leader enrollment.team_leader
json.spin_off enrollment.spin_off
json.accepted enrollment.accepted