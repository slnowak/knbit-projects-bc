json.partial! @project
json.memberships @project.memberships, partial: 'memberships/membership', as: :membership
json.enrollments @project.enrollments do |enrollment|
  json.id enrollment.id
  json.team_leader enrollment.team_leader
  json.spin_off enrollment.spin_off
  json.accepted enrollment.accepted
  json.application_form_id enrollment.application_form_id
  json.user do
    json.id enrollment.user(@user_store).id
    json.first_name enrollment.user(@user_store).first_name
    json.last_name enrollment.user(@user_store).last_name
    json.email enrollment.user(@user_store).email
    json.department_name enrollment.user(@user_store).department_name
    json.index_number enrollment.user(@user_store).index_number
    json.start_of_studies_year enrollment.user(@user_store).start_of_studies_year
  end
end