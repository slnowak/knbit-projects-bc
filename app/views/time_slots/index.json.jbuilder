json.time_slots @time_slots.each do |time_slot|
  json.partial! time_slot
  if time_slot.application_form.present?
    json.application_form do json.partial! time_slot.application_form end
  else
    json.application_form nil
  end
end