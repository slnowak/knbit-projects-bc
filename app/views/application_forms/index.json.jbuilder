# json.application_forms @application_forms, partial: 'application_forms/application_form', as: :application_form
json.application_forms @application_forms.each do |application_form|
  json.id application_form.id
  json.user_id application_form.user(@user_store).id
  json.user do
    json.id application_form.user(@user_store).id
    json.first_name application_form.user(@user_store).first_name
    json.last_name application_form.user(@user_store).last_name
    json.email application_form.user(@user_store).email
    json.department_name application_form.user(@user_store).department_name
    json.index_number application_form.user(@user_store).index_number
    json.start_of_studies_year application_form.user(@user_store).start_of_studies_year
  end
  json.status application_form.status
  json.notes application_form.notes
  json.inauguration_feedback application_form.inauguration_feedback
  json.created_at application_form.created_at
  json.enrollments application_form.enrollments.each do |enrollment|
    json.id enrollment.id
    json.project do
      json.id enrollment.project.id
      json.name enrollment.project.name
      json.description enrollment.project.description
      json.status enrollment.project.status
    end
    json.team_leader enrollment.team_leader
    json.spin_off enrollment.spin_off
    json.accepted enrollment.accepted
  end
  if application_form.meeting_time.present?
    json.meeting_time do
      json.id application_form.meeting_time.id
      json.start_time application_form.meeting_time.start_time
      json.end_time application_form.meeting_time.end_time
    end
  else
    json.meeting_time nil
  end
end