class User
  include ActiveModel::Model

  attr_accessor :id, :email, :first_name, :last_name, :department_name, :index_number, :start_of_studies_year
end