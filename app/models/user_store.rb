class UserStore
  attr_reader :users

  def initialize
    @users = UserService.fetch_collection
  end

  def fetch(user_id)
    users[user_id]
  end
end