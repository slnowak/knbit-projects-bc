class Membership < ActiveRecord::Base
  enum role: {developer: 0, team_leader: 1}
  enum status: {active: 0, inactive: 0}

  belongs_to :project

  validates :user_id, :project_id, :role, presence: true
  validates :user_id, uniqueness: {scope: :project_id}
end