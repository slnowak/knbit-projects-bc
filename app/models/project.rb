class Project < ActiveRecord::Base
  enum status: {not_started: 0, in_progress: 1, archived: 2}

  validates :name, :description, presence: true

  has_many :memberships
  has_many :enrollments
end