require 'rest-client'

class AuthService
  SERVER = 'knbitaa:8080'
  HEADERS = {'Content-Type' => 'application/json'}
  TOKEN_HEADER = 'knbit-aa-auth'

  OK_CODE = 200

  def self.authenticate(token)
    return false if token.blank?

    payload = {token: token}.to_json
    url = "#{SERVER}/authenticate"
    response = JSON.parse(RestClient.post(url, payload, HEADERS))

    response['userId']
  rescue RestClient::Forbidden
    false
  end

  def self.authorize(token, role)
    return false if token.blank? || role.blank?

    payload = {permission: role}.to_json
    headers = HEADERS.merge({TOKEN_HEADER => token})
    url = "#{SERVER}/authorize"
    response = RestClient.post(url, payload, headers)

    response.code == OK_CODE
  rescue RestClient::Forbidden, RestClient::Unauthorized
    false
  end
end
