require 'rest-client'

class UserService
  SERVER = 'membersbc:8080/users'
  HEADERS = {'Content-Type' => 'application/json'}
  TOKEN_HEADER = 'knbit-aa-auth'

  def self.prepare(user_id, token)
    @user_id = user_id
    @token = token
  end

  def self.fetch_collection
    url = SERVER
    headers = HEADERS.merge({TOKEN_HEADER => @token})

    Rails.logger.info("GET #{url}")
    response = JSON.parse(RestClient.get(url, headers))

    response['values'].inject({}) { |acc, r| acc[r['userId']] = User.new(parse_user_response(r)); acc }
  end

  def self.fetch(user_id)
    user_id ||= @user_id
    return false if user_id.blank?

    url = "#{SERVER}/#{user_id}"
    headers = HEADERS.merge({TOKEN_HEADER => @token})

    Rails.logger.info("GET #{url}")
    response = JSON.parse(RestClient.get(url, headers))

    User.new(parse_user_response(response))
  rescue RestClient::Forbidden, RestClient::Unauthorized, RestClient::ResourceNotFound
    false
  end

  private

  def self.parse_user_response(response)
    {
      id: response['userId'],
      email: response['email'],
      first_name: response['firstName'],
      last_name: response['lastName'],
      department_name: response['departmentName'],
      index_number: response['indexNumber'],
      start_of_studies_year: response['startOfStudiesYear']
    }
  end
end
