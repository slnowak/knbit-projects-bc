Rails.application.routes.draw do
  with_options defaults: {format: :json} do |r|
    r.resources :projects, except: [:new, :edit] do
      patch :change_status, on: :member
      resources :memberships, only: [:index, :create, :update, :destroy]
    end
    r.resources :application_forms, except: [:edit] do
      patch :set_meeting, on: :member
      patch :change_status, on: :member
      get :current, on: :collection
      resources :enrollments, only: [] do
        post :accept, on: :member
        post :reject, on: :member
      end
    end
    r.resources :time_slots, except: [:new, :edit] do
      post :create_day, on: :collection
    end
    r.resources :meeting_questions, except: [:new, :edit]
  end
end
