module JsonApiHelper
  def json_response
    @json_response ||= JSON.parse(body, symbolize_names: true)
  end
end
