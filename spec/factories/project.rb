FactoryGirl.define do
  factory :project do
    name { Faker::Company.catch_phrase }
    description { Faker::Lorem.paragraph }
    status :not_started
  end
end
