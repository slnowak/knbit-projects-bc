FactoryGirl.define do
  factory :user do
    skip_create

    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    email { Faker::Internet.email }
    id { SecureRandom.uuid }
  end
end
