FactoryGirl.define do
  factory :application_form do
    user_id { SecureRandom.uuid }
    status :pending
    notes { Faker::Lorem.paragraph }

    factory :application_form_with_enrollments do
      transient do
        enrollments_count 5
      end

      after(:create) do |application_form, evaluator|
        create_list(:enrollment, evaluator.enrollments_count, application_form: application_form)
      end
    end
  end
end
