require 'spec_helper'

RSpec.describe 'Projects', type: :request do
  let(:subject) { ProjectsController.new }
  before do
    subject.class.skip_before_filter :authenticate_user
    subject.class.skip_before_filter :authorize_admin
    allow(UserService).to receive(:fetch).and_return(User.new)
    allow(UserService).to receive(:fetch_collection).and_return({1 => User.new})
  end

  describe 'GET ' do
    let(:request) { get projects_path }
    let(:projects_response) { json_response[:projects] }

    it 'returns status 200' do
      request
      expect(response).to be_success
    end

    context 'when there is no projects' do
      it 'returns empty list' do
        request
        expect(projects_response.length).to eq(0)
      end
    end

    context 'when projects exist' do
      before { create_list(:project, 10) }
      it 'returns all projects' do
        request
        expect(projects_response.length).to eq(Project.count)
      end
    end
  end

  describe 'GET /:id' do
    let(:project) { create(:project) }
    let(:request) { get project_path(project_id) }

    context 'when project doesnt exist' do
      let(:project_id) { Project.last.id + 1 rescue 1 }
      it 'returns status 404' do
        request
        expect(response).to be_not_found
      end
    end

    context 'when project exists' do
      let(:project_id) { project.id }
      before { request }

      it 'returns status 200' do
        expect(response).to be_success
      end

      it 'returns proper object' do
        attributes = [:id, :name, :description, :status, :memberships, :enrollments]

        expect(json_response.length).to eq(attributes.length)
        attributes.each do |attr|
          expect(json_response[attr]).to eq(project.send(attr))
        end
      end
    end
  end

  describe 'POST /' do
    let(:request) { post projects_path, params }

    context 'missing parameters' do
      let(:params) { { project: attributes_for(:project).except(:name) } }


      it 'does not create event record' do
        expect { request }.not_to change { Project.count }
      end

      it 'returns status 422' do
        request
        expect(response).to be_unprocessable
      end

      it 'renders proper errors' do
        request
        expect(json_response[:errors][:name]).to_not be_nil
      end
    end

    context 'correct parameters' do
      let(:params) { { project: attributes_for(:project) } }

      it 'creates project' do
        expect { request }.to change { Project.count }.by(1)
      end

      it 'returns status 200' do
        request
        expect(response).to be_success
      end

      it 'renders proper template' do
        request
        expect(response).to render_template(:show)
      end
    end
  end

  describe 'PATCH /:id' do
    let(:request) { patch project_path(project_id), project: project_params }
    let(:name) { 'Project Name' }
    let(:new_name) { 'New Project Name' }
    let!(:project) { create(:project, name: name )}
    let(:project_attributes) { project.attributes.symbolize_keys.except(:id) }

    context 'when project doesnt exist' do
      let(:project_id) { Project.last.id + 1 rescue 1 }
      let(:project_params) { project_attributes }

      it 'returns status 404' do
        request
        expect(response).to be_not_found
      end
    end

    context 'when project exists' do
      let(:project_id) { project.id }

      context 'missing parameters' do
        let(:project_params) { project_attributes.merge(name: nil) }

        it 'does not create event record' do
          expect { request }.not_to change { project.name }
        end

        it 'returns status 422' do
          request
          expect(response).to be_unprocessable
        end

        it 'renders proper errors' do
          request
          expect(json_response[:errors][:name]).to_not be_nil
        end
      end

      context 'correct parameters' do
        let(:project_params) { project_attributes.merge(name: new_name) }

        it 'updates project' do
          expect { request }.to change { project.reload; project.name }.from(name).to(new_name)
        end

        it 'returns status 200' do
          request
          expect(response).to be_success
        end

        it 'renders proper template' do
          request
          expect(response).to render_template(:show)
        end
      end
    end
  end

  describe 'DESTROY /:id' do
    let!(:project) { create(:project) }
    let(:request) { delete project_path(project_id) }

    context 'when project doesnt exist' do
      let(:project_id) { Project.last.id + 1 rescue 1 }
      let(:project_params) { project_attributes }

      it 'returns status 404' do
        request
        expect(response).to be_not_found
      end
    end

    context 'when project exists' do
      let(:project_id) { project.id }

      it 'returns status 200' do
        request
        expect(response).to be_success
      end

      it 'destroys object' do
        expect { request }.to change { Project.count }.by(-1)
      end
    end
  end
end
