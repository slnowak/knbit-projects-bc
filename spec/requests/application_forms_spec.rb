require 'spec_helper'

RSpec.describe 'ApplicationForms', type: :request do
  let(:user) { create(:user) }
  let(:subject) { ApplicationFormsController.new }
  before do
    subject.class.skip_before_filter :authenticate_user
    subject.class.skip_before_filter :authorize_admin
    allow_any_instance_of(ApplicationForm).to receive(:user).and_return(user)
    allow(UserService).to receive(:fetch).and_return(User.new)
    allow(UserService).to receive(:fetch_collection).and_return({1 => User.new})
  end

  describe 'GET ' do
    let(:request) { get application_forms_path }
    let(:application_form_response) { json_response[:application_forms] }

    it 'returns status 200' do
      request
      expect(response).to be_success
    end

    context 'when there is no application_forms' do
      it 'returns empty list' do
        request
        expect(application_form_response.length).to eq(0)
      end
    end

    context 'when application_forms exist' do
      before { create_list(:application_form, 10) }
      it 'returns all application_forms' do
        request
        expect(application_form_response.length).to eq(ApplicationForm.count)
      end
    end
  end

  describe 'GET /:id' do
    let(:application_form) { create(:application_form_with_enrollments, enrollments_count: 2) }
    let(:request) { get application_form_path(application_form_id) }

    context 'when application_form doesnt exist' do
      let(:application_form_id) { ApplicationForm.last.id + 1 rescue 1 }
      it 'returns status 404' do
        request
        expect(response).to be_not_found
      end
    end

    context 'when application_form exists' do
      let(:application_form_id) { application_form.id }
      let(:enrollments_hash) do
        enrollments_attributes = [:id, :team_leader, :spin_off, :status]
        project_attributes = [:id, :name, :description, :status]

        application_form.enrollments.map do |e|
          project_hash = e.project.attributes.symbolize_keys.select { |k, _| project_attributes.include?(k) }
          e.attributes.symbolize_keys.select { |k, _| enrollments_attributes.include?(k) }.merge(project: project_hash)
        end
      end

      before { request }

      it 'returns status 200' do
        expect(response).to be_success
      end

      it 'returns proper object' do
        pending('Use json schema instead.')
        attributes = [:id, :user_id, :status]

        expect(json_response.length).to eq(attributes.length + 1)
        attributes.each do |attr|
          expect(json_response[attr]).to eq(application_form.send(attr))
        end


        expect(json_response[:enrollments]).to eq(enrollments_hash)
      end
    end
  end

  describe 'POST /' do
    let(:request) { post application_forms_path, params }

    context 'missing parameters' do
      let(:params) { {application_form: attributes_for(:application_form).except(:user_id)} }


      it 'does not create record' do
        expect { request }.not_to change { ApplicationForm.count }
      end

      it 'returns status 422' do
        request
        expect(response).to be_unprocessable
      end

      it 'renders proper errors' do
        request
        expect(json_response[:errors][:user_id]).to_not be_nil
      end
    end

    context 'correct parameters' do
      let(:enrollments_count) { 3 }

      # TODO - fix this hacky bang!
      let!(:enrollments_params) do
        skipped_params = [:id, :application_form_id, :created_at, :updated_at]
        params = []
        enrollments_count.times { params << build(:enrollment).attributes.symbolize_keys.except(*skipped_params) }
        params
      end

      let(:params) { {application_form: attributes_for(:application_form).merge(enrollments_attributes: enrollments_params,
                                                                                user_id: user.id)} }

      it 'creates application_form' do
        expect { request }.to change { ApplicationForm.count }.by(1)
      end

      it 'creates enrollments' do
        expect { request }.to change { Enrollment.count }.by(enrollments_count)
      end

      it 'returns status 200' do
        request
        expect(response).to be_success
      end

      it 'renders proper template' do
        request
        expect(response).to render_template(:show)
      end
    end
  end
end
